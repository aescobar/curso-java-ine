package gob.ine.cognos.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/hola")
public class HolaServlet extends HttpServlet{

	@Override
	public void init() throws ServletException {
		super.init();
		System.out.println("Iniciando HolaServlet");
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Llamando get");
		String nombre = req.getParameter("nombre");
		PrintWriter out = resp.getWriter();
		out.println("<HTML><HEAD><TITLE>Leyendo parámetros</TITLE></HEAD>");
		out.println("<BODY BGCOLOR=\"#CCBBAA\">");
		out.println("<H2>Leyendo parámetros desde un formulario html</H2><P>");
		out.println("<UL>\n");
		out.println("Te llamas " + req.getParameter("nombre") + "<BR>");
		out.println("</BODY></HTML>");
		out.close();
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("Llamando get");
		String nombre = req.getParameter("nombre");
		PrintWriter out = resp.getWriter();
		out.println("<HTML><HEAD><TITLE>Leyendo parámetros</TITLE></HEAD>");
		out.println("<BODY BGCOLOR=\"#CCBBAA\">");
		out.println("<H2>Leyendo parámetros desde un formulario html POST </H2><P>");
		out.println("<UL>\n");
		out.println("Te llamas " + req.getParameter("nombre") + "<BR>");
		out.println("</BODY></HTML>");
		out.close();
	}
	
}
