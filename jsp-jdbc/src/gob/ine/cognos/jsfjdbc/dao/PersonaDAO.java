package gob.ine.cognos.jsfjdbc.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import gob.ine.cognos.jsfjdbc.model.Persona;

public class PersonaDAO {

	public static final int SQL_OK = 1;
	public static final int SQL_ERROR = 2;
	
	private static Connection getConnection() {
		Connection con = null;
		try {
			Class.forName("org.postgresql.Driver");
			con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres?currentSchema=curso_java", "postgres", "patito");
		} catch (ClassNotFoundException e) {
			System.out.println("Error al cargar Driver " + e.getMessage());
		} catch (SQLException e) {
			System.out.println("No se pudo abrir la coneccion " + e.getMessage());
		}
		
		return con;
	}
	
	public static List<Persona> listar(){
		List<Persona> personas = new ArrayList<Persona>();
		Connection con = getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		if(con != null) {
			String sql = "SELECT * FROM persona"; 
			try {
				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();
				while(rs.next()) {
					Persona persona = new Persona();
					persona.setId(rs.getInt("id"));
					persona.setNombre(rs.getString("nombre"));
					persona.setEmail(rs.getString("email"));
					persona.setGenero(rs.getString("genero"));
					persona.setEdad(rs.getInt("edad"));
					personas.add(persona);
				}
			} catch (SQLException e) {
				System.out.println("La consulta no pudo ejecutarse" + e.getMessage());
			} finally {
				if(rs != null) {
					try {
						rs.close();
					} catch (SQLException e) {
						System.out.println("Error al cerrar RS" + e.getMessage());
					}
				}
				if(ps != null) {
					try {
						ps.close();
					} catch (SQLException e) {
						System.out.println("Error al cerrar RS" + e.getMessage());
					}
				}
				
				if(con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						System.out.println("Error al cerrar la coneccion" + e.getMessage());
					}
				}
			}
		}else {
			System.out.println("No se puido abrir la coneccion en el listado");
		}	
		return personas;
	}
	
	public static Persona obtenerPorId(int id){
		Persona persona = null;
		Connection con = getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		if(con != null) {
			String sql = "SELECT * FROM persona WHERE id = ?"; 
			try {
				ps = con.prepareStatement(sql);
				ps.setInt(1, id);
				rs = ps.executeQuery();
				while(rs.next()) {
					persona = new Persona();
					persona.setId(rs.getInt("id"));
					persona.setNombre(rs.getString("nombre"));
					persona.setEmail(rs.getString("email"));
					persona.setGenero(rs.getString("genero"));
					persona.setEdad(rs.getInt("edad"));
					
				}
			} catch (SQLException e) {
				System.out.println("La consulta no pudo ejecutarse" + e.getMessage());
			} finally {
				if(rs != null) {
					try {
						rs.close();
					} catch (SQLException e) {
						System.out.println("Error al cerrar RS" + e.getMessage());
					}
				}
				if(ps != null) {
					try {
						ps.close();
					} catch (SQLException e) {
						System.out.println("Error al cerrar RS" + e.getMessage());
					}
				}
				
				if(con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						System.out.println("Error al cerrar la coneccion" + e.getMessage());
					}
				}
			}
		}else {
			System.out.println("No se pudo abrir la coneccion en el listado");
		}	
		return persona;
	}
	
	
	public static int guardar(Persona persona){
		Connection con = getConnection();
		PreparedStatement ps = null;
		int resp = SQL_ERROR;
		if(con != null) {
			String sql = "INSERT INTO persona(nombre, email, genero, edad) values(?,?,?,?) "; 
			try {
				ps = con.prepareStatement(sql);
				ps.setString(1, persona.getNombre());
				ps.setString(2, persona.getEmail());
				ps.setString(3, persona.getGenero());
				ps.setInt(4, persona.getEdad());
				resp = ps.executeUpdate();
				
			} catch (SQLException e) {
				System.out.println("La consulta no pudo ejecutarse" + e.getMessage());
			} finally {
				if(ps != null) {
					try {
						ps.close();
					} catch (SQLException e) {
						System.out.println("Error al cerrar RS" + e.getMessage());
					}
				}
				
				if(con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						System.out.println("Error al cerrar la coneccion" + e.getMessage());
					}
				}
			}
		}else {
			System.out.println("No se pudo abrir la coneccion en el listado");
		}	
		return resp;
	}
	
	
	public static int modificar(Persona persona){
		Connection con = getConnection();
		PreparedStatement ps = null;
		int resp = SQL_ERROR;
		if(con != null) {
			String sql = "UPDATE persona SET nombre = ?, email = ?, genero = ?, edad = ? where id = ?"; 
			try {
				ps = con.prepareStatement(sql);
				ps.setString(1, persona.getNombre());
				ps.setString(2, persona.getEmail());
				ps.setString(3, persona.getGenero());
				ps.setInt(4, persona.getEdad());
				ps.setInt(5,persona.getId());
				resp = ps.executeUpdate();
				
			} catch (SQLException e) {
				System.out.println("La consulta no pudo ejecutarse" + e.getMessage());
			} finally {
				if(ps != null) {
					try {
						ps.close();
					} catch (SQLException e) {
						System.out.println("Error al cerrar RS" + e.getMessage());
					}
				}
				
				if(con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						System.out.println("Error al cerrar la coneccion" + e.getMessage());
					}
				}
			}
		}else {
			System.out.println("No se pudo abrir la coneccion en el listado");
		}	
		return resp;
	}
	
	public static int eliminar(Persona persona){
		Connection con = getConnection();
		PreparedStatement ps = null;
		int resp = SQL_ERROR;
		if(con != null) {
			String sql = "DELETE FROM persona WHERE id = ?"; 
			try {
				ps = con.prepareStatement(sql);			
				ps.setInt(1,persona.getId());
				resp = ps.executeUpdate();
				
			} catch (SQLException e) {
				System.out.println("La consulta no pudo ejecutarse" + e.getMessage());
			} finally {
				if(ps != null) {
					try {
						ps.close();
					} catch (SQLException e) {
						System.out.println("Error al cerrar RS" + e.getMessage());
					}
				}
				
				if(con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						System.out.println("Error al cerrar la coneccion" + e.getMessage());
					}
				}
			}
		}else {
			System.out.println("No se pudo abrir la coneccion en el listado");
		}	
		return resp;
	}
	
	
}
