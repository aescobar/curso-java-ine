<%@page import="gob.ine.cognos.jsfjdbc.model.Persona"%>
<%@page import="gob.ine.cognos.jsfjdbc.dao.PersonaDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<%
	int id = Integer.parseInt(request.getParameter("id"));
	Persona persona = PersonaDAO.obtenerPorId(id);
%>
	Id: <%= persona.getId() %><br/>
	Nombre: <%= persona.getNombre() %><br/>
	e-mail: <%= persona.getEmail() %><br/>
	Genero: <%= persona.getGenero() %><br/>
	Edad: <%= persona.getEdad() %><br/>
	
</body>
</html>