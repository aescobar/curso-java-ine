<%@page import="gob.ine.cognos.jsfjdbc.dao.PersonaDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<jsp:useBean id="persona" class="gob.ine.cognos.jsfjdbc.model.Persona"></jsp:useBean>
	<jsp:setProperty property="*" name="persona"/>
	<%
		int res = PersonaDAO.guardar(persona);
		if(res == PersonaDAO.SQL_OK){
			response.sendRedirect("index.jsp");
		}else{
			response.sendRedirect("error.jsp");
		}
	
	%>
</body>
</html>