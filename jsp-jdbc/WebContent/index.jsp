
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>   
<%@page import="java.util.List"%>
<%@page import="gob.ine.cognos.jsfjdbc.model.Persona"%>
<%@page import="gob.ine.cognos.jsfjdbc.dao.PersonaDAO"%> 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%
		List<Persona> personas =  PersonaDAO.listar();
		request.setAttribute("personas", personas);
	%>
	
	<a href="agregarpersona.jsp">Agregar</a>
	<table>
		<tr>
			<td>Id</td>
			<td>Nombre</td>
			<td>Email</td>
			<td>Genero</td>
			<td>Edad</td>
			<td>Acciones</td>
		</tr>
		<c:forEach items="${personas}" var="p">
			<tr>
				<td>${p.getId()}</td>
				<td>${p.getNombre()}</td>
				<td>${p.getEmail()}</td>
				<td>${p.getGenero()}</td>
				<td>${p.getEdad()}</td>
				<td>
					<a href="detallepersona.jsp?id=${p.getId()}">Detalle</a>
					<a href="modificarpersona.jsp?id=${p.getId()}">Modificar</a>
					<a href="eliminarpersona.jsp?id=${p.getId()}">Eliminar</a>
				</td>
			</tr>		
		</c:forEach>
	</table>
	<a href="descargar">Reporte</a>
</body>
</html>