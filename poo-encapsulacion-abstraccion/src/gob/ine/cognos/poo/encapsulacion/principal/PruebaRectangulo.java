package gob.ine.cognos.poo.encapsulacion.principal;

import gob.ine.cognos.poo.encapsulacion.objetos.Rectangulo;

/*
 *Crear una clase Rectángulo, con atributos base y altura que no 
 *deben ser accedidos de forma directa. Crear también el constructor 
 *de la clase y los métodos necesarios para calcular el área y el perímetro. 
 *Crear otra clase PruebaRectangulo que pruebe varios rectángulos y muestre 
 *por pantalla sus áreas y perímetros.  
 * */
 


public class PruebaRectangulo {

	public static void main(String[] args) {
		Rectangulo rectangulo1 = new Rectangulo();
		rectangulo1.setBase(10);	
		rectangulo1.setAltura(8);
		System.out.println("RECTANGULO 1:");
		System.out.println("AREA = " + rectangulo1.calcularArea());
		System.out.println("PERIMETRO = " + rectangulo1.calcularPerimetro());
		
		
		Rectangulo rectangulo2 = new Rectangulo();
		rectangulo2.setBase(25);	
		rectangulo2.setAltura(12);
		System.out.println("RECTANGULO 2:");
		System.out.println("AREA = " + rectangulo2.calcularArea());
		System.out.println("PERIMETRO = " + rectangulo2.calcularPerimetro());
		
		
		Rectangulo rectangulo3 = new Rectangulo();
		rectangulo3.setBase(45);	
		rectangulo3.setAltura(16);
		System.out.println("RECTANGULO 3:");
		System.out.println("AREA = " + rectangulo3.calcularArea());
		System.out.println("PERIMETRO = " + rectangulo3.calcularPerimetro());
		
		

	}

}
