package gob.ine.cognos.excepciones.objetos;

public class ValidacionException extends Exception{

	private int codError;
	
	public ValidacionException() {
		super();
		
	}

	public ValidacionException(String arg0) {
		super(arg0);
		
	}
	
	public ValidacionException(String arg0, int codError) {
		super(arg0);
		this.codError = codError;
		
	}

	public int getCodError() {
		return codError;
	}

	public void setCodError(int codError) {
		this.codError = codError;
	}
	
	

}
