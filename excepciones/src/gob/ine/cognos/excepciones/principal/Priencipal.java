package gob.ine.cognos.excepciones.principal;

import java.util.Scanner;

import gob.ine.cognos.excepciones.objetos.ValidacionException;

public class Priencipal {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String nombre;
		String apellido;
		int edad;
		
		nombre = sc.nextLine();
		apellido = sc.nextLine();
		edad = sc.nextInt();
		
		int a = 5;
		int b = 0;
		int r;
		
		
		try {
			if(validarCadena(nombre)) {
				System.out.println("Nombre correcto");
			}
			r = a / b;
		}catch(ValidacionException e) {
			System.out.println("Error en la validacion del nombre");
			System.out.println(e.getCodError() + " - " + e.getMessage());
		}catch(Exception e) {
			
			System.out.println("Error division");
		}
		
		try {
			if(validarCadena(apellido)) {
				System.out.println("Apellido correcto");
			}
		}catch(ValidacionException e) {
			System.out.println("Error en la validacion del apellido");
		}
		
		try {
			if(validarEntero(edad)) {
				System.out.println("Edad correcta");
			}
		}catch(ValidacionException e) {
			System.out.println("Error en la validacion de la edad ");
		}
		
		
		
	}
	
	private static boolean validarCadena(String cadena) throws ValidacionException {
		if(cadena.length() < 3 || cadena.length() > 15)
			throw new ValidacionException("Error validacion cadena", 285);		
		else 
			return true;
	} 
	
	private static boolean validarEntero(int entero) throws ValidacionException {
		if(entero < 0 || entero > 150)
			throw new ValidacionException();		
		else 
			return true;
	} 

}
