package gob.ine.cognos.javadoc.calculadora;

/**
 * Clase que sirve para realizar calculos
 * 
 * @author Ariel Escobar
 *
 */
public class Calculadora {

	/**
	 * Atributo para sumar el numero1
	 */
	private int numero1;
	/**
	 * Atributo para sumar el numero2
	 */
	private int numero2;
	
	/**
	 * Contructor sin parámetros para inicializar la calculadora
	 */
	public Calculadora() {
		
	}
	
	/**
	 * @param numero1 Parametro numero 1 del constructor
	 * @param numero2 Parametro numero 2 del constructor
	 */
	public Calculadora(int numero1, int numero2) {
		super();
		this.numero1 = numero1;
		this.numero2 = numero2;
	}

	public int getNumero1() {
		return numero1;
	}

	public void setNumero1(int numero1) {
		this.numero1 = numero1;
	}

	public int getNumero2() {
		return numero2;
	}

	public void setNumero2(int numero2) {
		this.numero2 = numero2;
	}
	
	/**
	 * @param numeroSumando Numero sumando
	 * @param etiqueta etiqueta descriptiva
	 * @return Retorna valor entero que es la suma del sumando mas los otros dos numeros declarados
	 */
	public int calcular(int numeroSumando, String etiqueta) {
		System.out.println(etiqueta + (numero1 + numero2 + numeroSumando));
		return numero1 + numero2 + numeroSumando;
	}
	
	
	
}
