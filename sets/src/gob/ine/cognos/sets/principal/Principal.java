package gob.ine.cognos.sets.principal;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import gob.ine.cognos.sets.objetos.Producto;

public class Principal {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int op;
		char respuesta;
		Set<Producto> productos = new HashSet<>();
		//List productos = new ArrayList();
		do {
			System.out.println("Ingrese los datos del producto");
			System.out.println("Nombre producto: ");
			String nombre = sc.nextLine();
			System.out.println("Precio producto: ");
			double precio = sc.nextDouble();
			System.out.println("Cantidad producto: ");
			int cantidad = sc.nextInt();
			
			Producto producto = new Producto(nombre, precio, cantidad);
			
			productos.add(producto);
			
			System.out.println("Desea seguir adicionando productos? S/N");
			String cad = sc.nextLine();
			cad = sc.nextLine();
			respuesta = cad.charAt(0);
		}while(respuesta == 'S'); 
	
		
		System.out.println("La lista de productos es"); 
		listarProductos(productos);
		
		System.out.println("Desea eliminar productos? S/N");
		String cad = sc.nextLine();
		respuesta = cad.charAt(0);
		
		while(respuesta == 'S') {
			System.out.println("Indique el número de producto que desa eliminar");
			listarProductos(productos);
			op = sc.nextInt();
			
			productos.remove(op);
			
			System.out.println("Desea eliminar productos? S/N");
			cad = sc.nextLine(); 
			if(cad.length() == 0 )
				cad = sc.nextLine(); 
			respuesta = cad.charAt(0);
		}
		
		listarProductos(productos);
		System.out.println("TOTAL " + precioTotal(productos));
		
	}
	
	private static  void listarProductos(Set<Producto> productos) {
		int c = 1;
		System.out.println("Nº \t  Producto \t Precio \t Cantidad");
		for(Producto producto : productos) {
			System.out.println(c + "\t" + producto.getNombre() + "\t" + producto.getPrecio() + "\t" + producto.getCantidad());
			c++;
		}
	}
	
	private static double precioTotal(Set<Producto> productos) {
		double res = 0;
		for(Producto producto : productos) {
			res = res +  (producto.getPrecio() * producto.getCantidad());
		}
		return res;
	}

}
