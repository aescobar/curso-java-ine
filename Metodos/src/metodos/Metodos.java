/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metodos;

import java.util.Scanner;

/**
 *
 * @author JAVA
 */
public class Metodos {

    /**
     * @param args the command line arguments
     */
    /* Crea un programa en Java que calcule el costo del 
estacionamiento en un parqueo de modo que las 3 primeras
horas se cobran a 7 bolivianos y las siguientes a 2 bolivianos 
por hora hasta llegar a un tope de 24 horas. Se da por hecho 
que el usuario introducirá un número entero de horas y debe 
obtener el precio.
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);

        int numHoras, precio;
        
        System.out.println("Ingrese la cantidad de horas: ");
        numHoras = sc.nextInt();
        System.out.print("El precio a pagar es: ");
        precio = calcularParqueo(numHoras);
        if(precio == -1)
            System.out.println("Limite de tiempo");
        else
            System.out.println(precio + " Bs.");
    }
    
    public static int calcularParqueo(int numHoras){
        if (numHoras <= 24) {
            
            if (numHoras <= 3) {
                return 7;                
            }else{
                int restoHoras = numHoras - 3;
                int total = 7 + restoHoras * 2;
                return total;
            }
        } else {
            return -1;
        }
    }

}
