package gob.ine.cognos.archivos.principal;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Escritor {
	public static void main(String[] args) {
		File archivo = new File("/home/java/archivo_escrito.txt");
		if (archivo.exists())
			System.out.println("Archivo existe");
		else {
			System.out.println("Archivo no existe");
			try {
				archivo.createNewFile();
				System.out.println("Archivo creado");
			} catch (IOException e) {
				System.out.println("Error al crear archivo");
			}
		}
		
		try {
			FileWriter fw = new FileWriter(archivo);
			PrintWriter pw = new PrintWriter(fw);
			pw.write("Texto prueba\n");
			
			pw.append("Segundo texto");
			pw.println();
			pw.format("Hola %s edad %d", "Roberto", 40);
			pw.println();
			pw.format("Numero decima %.4f", 25.896547);
			fw.close();
			System.out.println("Texto escrito");
		} catch (IOException e) {
			System.out.println("El archivo no puede ser leido");
		}
	}
}







