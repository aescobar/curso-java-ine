package gob.ine.cognos.poo.sobrecarga.principal;

import gob.ine.cognos.poo.sobrecarga.objetos.Cuenta;

public class Principal {

	public static void main(String[] args) {
		Cuenta cuenta = new Cuenta("Angela", 12345678);
		System.out.println("TITULAR: " + cuenta.getNombreTitular() + " NRO. CUENTA: " + cuenta.getNroCuenta());
		cuenta.modificar("Juan Carlos");
		System.out.println("TITULAR: " + cuenta.getNombreTitular() + " NRO. CUENTA: " + cuenta.getNroCuenta());
		cuenta.modificar(12345678);
		System.out.println("TITULAR: " + cuenta.getNombreTitular() + " NRO. CUENTA: " + cuenta.getNroCuenta());
		cuenta.modificar("Angela", 12345666);
		System.out.println("TITULAR: " + cuenta.getNombreTitular() + " NRO. CUENTA: " + cuenta.getNroCuenta());
	
	}
	
}
