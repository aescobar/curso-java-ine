package gob.ine.cognos.poo.sobrecarga.objetos;

public class Cuenta {

	private int nroCuenta;
	private String nombreTitular;
	
	
	
	public Cuenta() {
	}

	public Cuenta(String nombreTitular, int nroCuenta) {
		this.nroCuenta = nroCuenta;
		this.nombreTitular = nombreTitular;
	}

	public int getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(int nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public String getNombreTitular() {
		return nombreTitular;
	}

	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}
	
	public void modificar(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}
	
	public void modificar(String nombreTitular, int nroCuenta) {
		this.nombreTitular = nombreTitular;
		this.nroCuenta = nroCuenta;
	}
	
	public void modificar(int nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	
	
	
}
