package gob.ine.cognos.poo.polimorfismo.objetos;

public class Rectangulo extends Figura {
	
	private double ladoA;
	private double ladoB;
		
	public Rectangulo() {

	}

	public Rectangulo(double ladoA, double ladoB) {
		super();
		this.ladoA = ladoA;
		this.ladoB = ladoB;
	}

	public double getLadoA() {
		return ladoA;
	}

	public void setLadoA(double ladoA) {
		this.ladoA = ladoA;
	}

	public double getLadoB() {
		return ladoB;
	}

	public void setLadoB(double ladoB) {
		this.ladoB = ladoB;
	}

	@Override
	public double calcularArea() {
		return ladoA * ladoB;
	}
	
	
	

}
