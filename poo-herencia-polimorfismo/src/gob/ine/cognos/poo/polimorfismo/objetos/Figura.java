package gob.ine.cognos.poo.polimorfismo.objetos;

public abstract class Figura {
	
	public abstract double calcularArea();

}
