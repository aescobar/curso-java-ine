package gob.ine.cognos.poo.polimorfismo.objetos;

public class Cuadrado extends Figura{

	private double lado;
		
	public Cuadrado() {

	}

	public Cuadrado(double lado) {
		
		this.lado = lado;
	}

	public double getLado() {
		return lado;
	}

	public void setLadoA(double lado) {
		this.lado = lado;
	}

	@Override
	public double calcularArea() {
		return lado * lado;
	}
	
}
