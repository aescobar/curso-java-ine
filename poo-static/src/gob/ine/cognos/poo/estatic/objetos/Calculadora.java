package gob.ine.cognos.poo.estatic.objetos;

public class Calculadora {
	
	public static final double VALOR_MINIMO = -10000;

	public static double calcularPromedio(double[] array) {
		double sum = 0;
		for(int i = 0; i<array.length; i++) {
			sum += array[i];
		}
		return sum/array.length;
		
	}
	
	public static double obtenerMayor(double[] array) {
		double max = VALOR_MINIMO;
		for(int i = 0; i<array.length; i++) {
			if(array[i]> max)
				max = array[i];
		}
		return max;
		
	}
	
}
