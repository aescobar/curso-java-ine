package gob.ine.cognos.poo.estatic.principal;

import gob.ine.cognos.poo.estatic.objetos.Calculadora;

public class Principal {

	public static void main(String[] args) {
		double[] array = {10,2,4,8,1,3,25,6,17,42};
		double promedio = Calculadora.calcularPromedio(array);
		double max = Calculadora.obtenerMayor(array);
		
		System.out.println("EL PROMEDIO DEL VECTOR ES: " + promedio );
		System.out.println("EL NUMERO MAYOR DEL VECTOR ES: " + promedio );
		System.out.println("EL VALOR MIN ES: " + Calculadora.VALOR_MINIMO );
		
	}
	
}
