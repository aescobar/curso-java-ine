package gob.ine.cognos.swinginicio.principal;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GridLayoutEjemplo extends JFrame{
	
	private JButton jbBoton1;
	private JButton jbBoton2;
	private JButton jbBoton3;
	private JButton jbBoton4;
	
	public GridLayoutEjemplo() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		PanelPersonal panelPersonal = new PanelPersonal();
		panelPersonal.setBackground(Color.CYAN);
		setContentPane(panelPersonal);
		JPanel panelPrincipal = (JPanel)getContentPane();
		panelPrincipal.setLayout(new GridLayout(2,2));
		jbBoton1 = new JButton("BOTON 1");
		jbBoton2 = new JButton("BOTON 2");
		jbBoton3 = new JButton("BOTON 3");
		jbBoton4 = new JButton("BOTON 4");
		panelPrincipal.add(jbBoton1);
		panelPrincipal.add(jbBoton2);
		panelPrincipal.add(jbBoton3, 1);
		panelPrincipal.add(jbBoton4);
		
		pack();
	}
	
	public static void main(String[] args) {
		GridLayoutEjemplo grid = new GridLayoutEjemplo();
		grid.setVisible(true);
	}

}
