/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio3;

import java.util.Scanner;

/**
 *
 * @author JAVA
 */
/*Crea un programa en Java que solicite al usuario dos cadenas 
de caracteres y que devuelva la primera cadena, pero transformando 
en mayúsculas la parte que coincide con la segunda cadena. Por ejemplo, 
si se introducen las cadenas “Ejercicio con cadenas” y “cadena”, devolverá 
Ejercicio con CADENAs”.*/


public class Ejercicio3 {
    public static void main(String[] args){
        
        Scanner sc = new Scanner(System.in);
        String cadena;
        String cadena2;
        cadena = sc.nextLine();
        cadena2 = sc.nextLine();
        
        int p = cadena.indexOf(cadena2);
        if(p == -1){
            System.out.println("No existe la subcadena");
        }else{
            cadena = cadena.substring(0, p) + cadena2.toUpperCase() + cadena.substring(p + cadena2.length());
            System.out.println(cadena);
        }
    }
}
